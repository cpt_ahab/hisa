/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

fluxScheme           AUSMPlusUp;
lowMachAusm          false;

ddtSchemes
{
    default          bounded dualTime rPseudoDeltaT steadyState;
}

gradSchemes
{
    default          faceLeastSquares linear;
}

divSchemes
{
    default          none;
}

laplacianSchemes
{
    default          Gauss linear corrected;
}

interpolationSchemes
{
    default          linear;
    reconstruct(rho) wVanLeer;
    reconstruct(U)   wVanLeer;
    reconstruct(T)   wVanLeer;
}

snGradSchemes
{
    default          corrected;
}


// ************************************************************************* //
